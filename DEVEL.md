## Tests

The package includes a test interface that can compare the range
calculations from the current source for various canonical detector
ASDs against those calculated from an earlier git commit:

    $ python3 -m inspiral_range.test --help

This is useful to check if code changes are causing differences in
range calculations.


## Cached interpolation data

`inspiral-range` includes caches of interpolation data for a couple of
calculations that are either expensive or require special libraries.

### detector response

The `inspiral_range.ang_avg` module includes a function to calculate
the angle-averaged detector response for a given detection SNR.  This
is an expensive calculation, so we therefore include data needed to
create an interpolant of the function.  The data can be regenerated
by execution the module:
```shell
$ python3 -m inspiral_range.ang_avg
```

### waveform generation

The `inspiral_range.waveform` module includes functions for generating
strain amplitude data for various inspiral waveforms.  Raw waveform
generation is handled by the `lalsimulation` package.  In order to not
have a hard dependency on this package, we cache interpolant data for
a function that extrapolates waveform amplitude data for equal mass
inspirals.  The data can be regenerated by execution of the module:
```shell
$ python3 -m inspiral_range.waveform
```
